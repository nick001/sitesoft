# Sitesoft news parser

## Install
1) Clone repo localy
2) Go to project directory ```bash cd sitesoft```
2) You should have python version 3 (3.6.7 prefered) ```bash python --version```
3) Install all needed packages ```bash pip install -r requirements.txt```
4) All parsed pages will be saved in project folder, so each time you want to clear all you should manually delete all folders, and clear parsed.json file (this file should contains '{}' only for totally clear run)

## How to 
1) Go to project directory ```bash cd sitesoft```
2) You can change HEADERS and SITES_TEXT_CLASSES in settings.py (it is in todo list). HEADERS is used by requests headers. SITES_TEXT_CLASSES is used by 'cleaner' table to avoid garbage collecting from known sites
3) Call ```bash python cli.py``` to show cli help message (that message should be enough for start)

## TODO
1) Add delete command (delete by source), clear all command (delete all news and clear table), set settings command
2) Add multithreading or even multiprocessing
