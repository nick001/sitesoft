"""
Module which contain methods needed to parse news from different news sources.
Used settings module to get 'well' known news sources, headers (used as request
headers) and name of unique parsed news file (should be JSON)
Using by cli.py
"""

import datetime
import json
import os
from http.client import InvalidURL
from urllib import error, request
from urllib.parse import urlparse, urljoin

from bs4 import BeautifulSoup, SoupStrainer

from settings import PARSED_STORAGE, HEADERS, SITES_TEXT_CLASSES


def parse_many_urls_in_depth(urls, depth=2):
    """
    Method which parse given urls pages in depth. Each iteration get html from
    given url and all links was found on page. Then use founded links as given
    url, till depth != 0. Save news to files and update unique news table (if
    news added)

    :param urls: news sources urls to parse news in depth
    :param depth: how much parse iterations do through links,
        should be no negative
        0 - parse only given url page
        1 - parse given url page and all links found on that page, etc
    :type urls: tuple
    :type depth: int
    """

    # get already saved unique news
    with open(PARSED_STORAGE, 'r') as unique_news_file:
        unique_news = json.load(unique_news_file)

    # TODO maybe add multithreading or multiprocessing there
    # start many url in depth parsing
    for url in urls:
        # clear source url tail, no need slashes at end of url
        if url.endswith('/'):
            url = url[:-1]

        # parse given url in depth, saving news files, add unique news data to
        # table of unique news (to avoid parsing same news few times)
        parse_url_in_depth(url, unique_news, depth=depth)

    # update unique news table after parsing process
    with open(PARSED_STORAGE, 'w') as unique_news_file:
        json.dump(unique_news, unique_news_file, ensure_ascii=False)


def parse_url_in_depth(url, unique_news, depth=2):
    """
    Method which parse given url page in depth. Each iteration get html from
    given url and all links was found on page. Then use founded links as given
    url, till depth != 0. Save news to files and update unique news table (if
    news added)

    :param url: news source url
    :param unique_news: already parsed and stored unique news
    :param depth: how much parse iterations do through links,
        should be no negative
        0 - parse only given url page
        1 - parse given url page and all links found on that page, etc
    :type url: str
    :type unique_news: dict
    :type depth: int
    """

    # make first iteration to get given url html, and links to other pages
    unparsed_pages = single_page_parser(url, unique_news)
    parsed_pages = {url}
    root_url = urlparse(url).path

    # try in depth parsing
    while depth != 0:
        # links found on parsed page
        pages_links = set()
        for page_url in unparsed_pages:
            # already parsed links, avoid parsing same pages
            parsed_pages.add(page_url)

            # no need to parse link if link have different path than source url
            if root_url and root_url not in urlparse(page_url).path:
                continue

            # get unique links from parsed page
            pages_links.union(single_page_parser(page_url, unique_news))

        # unique not parsed urls found on page
        unparsed_pages = pages_links.difference(parsed_pages)
        depth -= 1


def single_page_parser(url, unique_news):
    """
    Try to get html by url, save news text if request successful (clean or raw
    html, depends on knowledge about news source), get all links to same source
    url from page.
    Return page all links to same news source url

    :param url: news source url
    :param unique_news: already parsed and stored unique news
    :return: links found on parsed page (to same news source url)
    :type url: str
    :type unique_news: dict
    :rtype: set
    """

    # get html code by news source url
    # you can add request headers at settings.py
    page_request = request.Request(url, headers=HEADERS)
    # catch some exceptions
    try:
        html = request.urlopen(page_request).read()
    except error.HTTPError:
        print('HTTP error: {}'.format(url))
        return set()
    except InvalidURL:
        print('bad port: {}'.format(url))
        return set()
    except error.URLError:
        print('URL error: {}'.format(url))
        return set()

    # save html to file if got response without exceptions
    print('now saving: {}'.format(url))
    save_html_to_file(html, url, unique_news)

    # get all links from html page, need to next depth iteration
    links = set()
    for link in BeautifulSoup(html, parse_only=SoupStrainer('a')):
        if link.has_attr('href'):
            href = link['href']
            # if relative link to same source.
            # remove it condition if you want parse different sources urls too
            if not href.startswith('http'):
                links.add(urljoin(url, href))

    return links


def save_html_to_file(html, url, unique_news):
    """
    Method try to clear news (if news source 'well' known) then generate new
    file (source use as folder name, news title use as file name) with news if
    news is unique (check all news table, by news title). If news not unique,
    or source 'well' known and page empty after cleaning, do nothing

    :param html: full html of news page
    :param url: news source url
    :param unique_news: already parsed and stored unique news
    :type html: bytes
    :type url: str
    :type unique_news: dict
    """

    soup = BeautifulSoup(html, 'html.parser')
    netloc = urlparse(url).netloc

    # try to get 'clear' news text, if news text 'well' known
    if netloc in SITES_TEXT_CLASSES.keys():
        clear_news = soup.findAll('div', {'class': SITES_TEXT_CLASSES[netloc]})
    else:
        clear_news = soup

    # no need to save page which have not 'clear' text, for 'well' known source
    if not clear_news:
        return

    # generate path for news file
    title = soup.title.text
    path = create_news_file_path(netloc, title)

    # add news source to table
    if netloc not in unique_news.keys():
        unique_news[netloc] = dict()

    # check title unique
    if title in unique_news[netloc].keys():
        return

    # save unique news to file
    with open(path, 'a') as new_news:
        for tag in clear_news:
            new_news.write(str(tag))

    # add unique news to table, use timestamp to sorting later
    news_parsed_timestamp = datetime.datetime.utcnow().isoformat()
    unique_news[netloc][title] = {'loaded': news_parsed_timestamp, 'url': url}


def create_news_file_path(source_net_location, title):
    """
    Create path for new news, use to save unique news.
    Create directory if it does not exists
    News title using as filename, news source url part
    (such as lenta.ru, ria.ru, etc) using as folder name.
    Returns relative path to file, which should be used to save news for

    :param source_net_location: news source url part using as directory name,
        (such as lenta.ru, ria.ru, etc)
    :param title: news title, using as file name
    :return: relative path to file which will contains new news
    :type source_net_location: str
    :type title: str
    :rtype: str
    """

    # OS independent relative path generation
    path = os.path.join(source_net_location, title)
    # make directory if it does not exists
    head, tail = os.path.split(path)
    make_directory(head)

    return path


def make_directory(path_to_directory):
    """
    Create directory ONLY if it does NOT EXISTS
    Do nothing if directory already exists
    Path to directory should be pass to method as string

    :param path_to_directory: path to directory which want to create
    :type path_to_directory: str
    """

    if not os.path.exists(path_to_directory):
        os.makedirs(path_to_directory)


if __name__ == '__main__':
    test_urls = (
        # 'well' known sources, know how to get clear text
        'https://echo.msk.ru/',
        'https://lenta.ru/',
        'https://ria.ru/',
        'https://www.vesti.ru/',
        'https://tass.ru/ural-news',  # using regional news only
        # unknown sources, DO NOT know how to get clear text
        'https://news.mail.ru/economics'
    )
    test_depth = 1
    parse_many_urls_in_depth(test_urls, depth=test_depth)
