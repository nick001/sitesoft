"""
Module with cli entrypoint, contains all methods used by cli. Use external
modules to change or add functionality. Add new methods here to add new
commands
"""

import click

from news_getter import get_many_news_by_url
from news_parser import parse_many_urls_in_depth


# TODO add delete and clear all commands
@click.group()
@click.pass_context
def parser(context):
    """
    Cli which can be used for parsing news sources in depth and shown parsed
    unique news. If you want clear all, delete all parsed news folders and make
    PARSED_STORAGE file contains only '{}'\n
    Use python cli.py load --help to read about parsing news.\n
    Use python cli.py get --help to read about how to get news from storage
    """
    pass


@parser.command('load')
@click.option(
    '--depth', '-d', default=2,
    help='number of layers to parse site.If set to 0 only given urls will'
         'be parsed. If set to 1 given urls will be parsed and all links on'
         'given urls will be parsed too. If set to 2 link on links urls will'
         'be parsed too, etc. Default = 2'
)
@click.argument('urls', nargs=-1, required=True)
@click.pass_obj
def parser_load(context, urls, depth):
    """
    Command to start parsing news sites. Try to search in depth on all given
    sites and then save news to files (html) and to storage (JSON) with title,
    parsing finished time, news source tree. If news source 'well' known you
    could add it to settings to avoid saving useless html. Pass news source
    urls with one space separator. Depth OPTION should be integer and
    positive\n
    Example: python cli.py load -d 1 https://lenta.ru https://ria.ru/
    """

    parse_many_urls_in_depth(urls, depth=depth)


@parser.command('get')
@click.option(
    '--number', '-n', default=2,
    help='number of news to return (per each source), should be positive (>0).'
         ' Default = 2'
)
@click.argument('urls', nargs=-1, required=True)
@click.pass_obj
def parser_get(context, urls, number):
    """
    Command to get parsed news from storage. News will be sorted by source
    name, time (oldest first) and number (set number OPTION, default=2). Pass
    news source urls with one space separator. Number of news should be integer
    and positive.\n
    Example: python cli.py get -n 100 https://lenta.ru https://ria.ru/
    """
    news = get_many_news_by_url(urls, number_of_news=number)

    for new in news:
        print(new)


if __name__ == '__main__':
    parser()
