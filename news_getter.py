"""
Module to load stored unique news from storage
Using by cli.py
"""

import json
from urllib.parse import urlparse

from settings import PARSED_STORAGE


def get_many_news_by_url(urls, number_of_news=2):
    """
    Method to get news from news storage by news sources urls (some iterable
    structure with urls as strings), sorted by time (oldest first) and return
    only few news.
    Optional argument is number of news to return, should be positive

    :param urls: news sources urls as strings
    :param number_of_news: number of news to return (oldest first),
        should be positive (>0)
    :return: list of sorted news (oldest first) with defined length, or
        return empty list if no news in storage
    :type urls: tuple
    :type number_of_news: int
    :rtype: list
    """

    many_urls_news = list()
    for url in urls:
        many_urls_news += get_news_by_url(url, number_of_news=number_of_news)

    return many_urls_news


def get_news_by_url(url, number_of_news=2):
    """
    Try to find news in storage file, by given url
    Return list of news (of string) which have following format:
    'news_source_url  news_title'. Url given as string.
    Optional argument make user able to choose how many news will be shawn
    It shows news from old to new, so oldest news will be shawn first

    :param url: news source url to find news in storage
    :param number_of_news: number of news to return (oldest first),
        should be positive (>0)
    :return: list of sorted news (oldest first) with defined length, or
        return empty list if no news in storage for given news source
    :type url: str
    :type number_of_news: int
    :rtype: list
    """

    # load all news from file
    with open(PARSED_STORAGE, 'r') as all_news_storage:
        all_parsed_news = json.load(all_news_storage)

    # check news source in storage
    news_source_location = urlparse(url).netloc
    if news_source_location not in all_parsed_news.keys():
        return list()

    # get news from storage by source
    # and sort it by downloading time, oldest first
    news_by_source = all_parsed_news[news_source_location]
    sorted_news = sorted(news_by_source.items(), key=lambda k: k[1]['loaded'])

    # make list with needed news (strings), sorted by source, time and count
    news = list()
    for title, meta in sorted_news[:number_of_news]:
        news.append('{}   {}'.format(meta['url'], title))

    return news


if __name__ == '__main__':
    test_urls = (
        'https://tass.ru/ural-news',
        'https://lenta.ru/',
        'https://echo.msk.ru/',
        'https://ria.ru/',
        'https://www.vesti.ru/'
    )
    test_number = 5
    test_news = get_many_news_by_url(test_urls, number_of_news=test_number)

    for new in test_news:
        print(new)
