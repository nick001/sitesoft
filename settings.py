"""
Module to save cli settings. Change it manually (change code) or better add cli
commands to change it values via console interface
"""

# will be used as headers when parsing news
HEADERS = {}
# name of file which contain table with parsed news (title, time, source)
PARSED_STORAGE = 'parsed.json'
# names of classes which contains usefull text blocks for some sites
# for given sites (keys) will be saved only it block (clean data)
# all other sites will be saved full (as is, without cleaning)
SITES_TEXT_CLASSES = {
    'www.vesti.ru': 'article__text',
    'ria.ru': 'article__text',
    'echo.msk.ru': 'typical include-relap-widget',
    'lenta.ru': 'b-text',
    'tass.ru': 'text-block'
}
